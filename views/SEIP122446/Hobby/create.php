<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobbby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select Your Hobby</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="usr">First Name:</label>
            <input type="text" name="first_name" class="form-control" id="usr">
        </div>
        <div class="form-group">
            <label for="usr">Last Name:</label>
            <input type="text" name="last_name" class="form-control" id="usr">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Gardening">Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Cycling">Cycling</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Traveling">Traveling</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="hobby[]" value="Swiming">Swiming</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>
