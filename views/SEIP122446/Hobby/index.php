<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP122446\Utility\Utility;
use App\Bitm\SEIP122446\Hobby\Hobby;
use App\Bitm\SEIP122446\Message\Message;

$hobby= new Hobby();
$allHobby=$hobby->index();
//Utility::d($allHobby);
?>


<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <h2>Hobby List</h2>

    <a href="create.php" class="btn btn-info" role="button">Add Hobby</a>  <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
    <br>
    <div id="message">

        <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
            echo Message::message();
        }?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Hobbies</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allHobby as $hobby){
                $sl++;
                ?>
                <tr>
                    <td><?php echo $sl; ?></td>
                    <td><?php echo $hobby['id'] // for object: $book->id ; ?></td>
                    <td><?php echo $hobby['firstname'] // for object: $book->title; ?></td>
                    <td><?php echo $hobby['lastname'] // for object: $book->title; ?></td>
                    <td><?php echo $hobby['hobbies'] // for object: $book->title; ?></td>
                    <td>
                        <a href="view.php?id=<?php echo $hobby['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
                        <a href="edit.php?id=<?php echo $hobby['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $hobby['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $hobby['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
                    </td>


                </tr>
            <?php } ?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>

</html>


