<?php
namespace App\Bitm\SEIP122446\Hobby;
use App\Bitm\SEIP122446\Utility\Utility;
use App\Bitm\SEIP122446\Message\Message;

class Hobby{
    public $id="";
    public $firstName="";
    public $lastName="";
    public $hobby="";
    public $conn;

    public function prepare($data=Array()){
        if(array_key_exists("first_name",$data)){
            $this->firstName= $data['first_name'];
        }
        if(array_key_exists("last_name",$data)){
            $this->lastName= $data['last_name'];
        }
        if(array_key_exists("hobby",$data)){
            $this->hobby= $data['hobby'];
        }

        if(array_key_exists("id",$data)){
            $this->hobby= $data['id'];
        }

        //Utility::dd($this->firstName);
        return $this;
    }

    public function __construct()
    {
        $this->conn= mysqli_connect("localhost", "root", "", "atomicprojectb21") or die("Database connection failed");
    }


    public function store(){
        $query= "INSERT INTO `atomicprojectb21`.`hobby` (`firstname`, `lastname`, `hobbies`) VALUES ('".$this->firstName."', '".$this->lastName."', '".$this->hobby."')";
        echo $query;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header('Location:index.php');
        }else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('index.php');
        }
    }

    public function index(){
        $_allHobby= array();
        $query="SELECT * FROM `atomicprojectb21`.`hobby`";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_allHobby[]=$row;
        }

        return $_allHobby;
    }
}